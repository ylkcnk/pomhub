module.exports = {
    inputgetName(e) {
        // 从事件对象 e 中获取输入框的 data-name 属性值
        let name = e.currentTarget.dataset.name;
        // 用于存储更新后的数据对象
        let nameMap = {};

        // 通过判断 name 是否包含点符号 . 来确定是否存在多层级的数据绑定关系
        if (name.indexOf('.') !== -1) {
            let nameList = name.split('.');
            // 判断多层级的第一个属性是否存在于 this.data 中，
            //如果存在，将其保存到 nameMap 中，
            //否则创建一个空对象并保存到 nameMap
            if (this.data[nameList[0]]) {
                nameMap[nameList[0]] = this.data[nameList[0]];
            } else {
                nameMap[nameList[0]] = {};
            }
            // 将输入框的值 e.detail.value 更新到 nameMap 中
            nameMap[nameList[0]][nameList[1]] = e.detail.value;
        } else {
            // 单层的数据直接更改
            nameMap[name] = e.detail.value;
        }
        this.setData(nameMap);
    }
};