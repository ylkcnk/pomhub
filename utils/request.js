const app = getApp();
import {
  login
} from '../api/user'
// var baseURL = app.getURL();//将url定义在app.js缓存中，直接赋值也是可以的
// var baseURL = 'http://127.0.0.1:8080'
var baseURL = 'http://192.168.245.50:8080'
// var baseURL = getApp().data.baseURL;
// var baseURL = 'http://localhost:8080' //将url定义在app.js缓存中，直接赋值也是可

var cookieToken = wx.getStorageSync('token') //本地数据从本地缓存中取出
// var cookieToken = 'eyJhbGciOiJIUzUxMiJ9.eyJ1c2VyX2lkIjoxMDcsInVzZXJfa2V5IjoiNGZmYTY2ZDctYTc4NC00YjBlLTg5NTEtNTk4ZWI4MmEwYmJlIiwidXNlcm5hbWUiOiJvalpMSDZ5em1EUXJoQ1pkRm04Umc5QWw1WTVVIn0.Cw9z9y5ZiEt115CAUmRYhmaBzEyqW9W-S9Hr-cdRiGLQ1r6qshlGGG9Y2DTfzUpSc8cB-FO-2CFbURnLSU0Rkg'

/**
 * 统一封装的请求
 * 
 * @param option为参数对象，至少包含url、method
 */
const Request = function request(option) {
  return new Promise(function (resolve, reject) {

    let header = {
      'content-type': 'application/json',
      // "Authorization": user.cookieToken //携带token在请求头中
      "Authorization": wx.getStorageSync('token') //携带token在请求头中
    };
    // if (header.Authorization === null) {
    //   header.Authorization = wx.getStorageSync('token')
    // }
    wx.request({
      url: baseURL + option.url,
      method: option.method,
      data: option.data === undefined ? '' : JSON.stringify(option.data), // 如果data没有传入，就给空串，不然就序列化
      header: header, //请求头
      timeout: 10000, //超时时间 10s
      success(res) {

        //请求成功,此处根据你的业务返回的状态码进行修改
        if (res.data.code === 200) {
          resolve(res);
        } else if (res.data.code === 500) {
          //登录状态失效，需要重新刷新数据
          reject('登录状态失效，需要重新刷新数据');
        } else if (res.data.code === 401) {
          //未登录
          reject('登录已经过期');
          login()

        } else {
          //其他异常
          reject('运行时错误,请稍后再试');
        }
      },
      fail(err) {
        //请求失败
        reject(err)
      }
    })
  })
}


//导出
module.exports = Request