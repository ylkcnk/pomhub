const request = require('../utils/request'); //引入刚刚编写的request 文件

/**
 * 在这里写具体的api
 */
const comment = {
  /**
      * 查询待办任务列表(子任务)
      */
  todolistChild() {
    return request({
      url: `/pgh/list/todolistChild`,
      method: 'GET'
    });
  },

  /**
   * 查询待办任务列表(任务|子任务)
   */
  todolist() {
    return request({
      url: `/pgh/list/todolist`,
      method: 'GET'
    });
  },

  /**
   * 查询所有
   */
  selectTodoList() {
    return request({
      url: `/pgh/list/list`,
      method: 'GET'
    });
  },

  // 查询待办任务详细
  getList(id) {
    return request({
      url: '/pgh/list/' + id,
      method: 'get'
    })
  },

  // 新增待办任务
  addList(data) {
    return request({
      url: '/pgh/list',
      method: 'post',
      data: data
    })
  },

  // 修改待办任务
  updateList(data) {
    return request({
      url: '/pgh/list',
      method: 'put',
      data: data
    })
  },

  // 增加次数
  addtimes(data) {
    return request({
      url: '/pgh/list/addTime',
      method: 'put',
      data: data
    })
  },


  // 删除待办任务
  delList(id) {
    return request({
      url: '/pgh/list/' + id,
      method: 'delete'
    })
  },
};
//导出
module.exports = comment