const request = require('../utils/request');

export function login() {
  wx.login({
    success(res) {
      if (res.code) {
        //发起网络请求
        request({
          url: '/auth/wxLogin',
          method: 'post',
          data: {
            code: res.code
          },
          headers: {}
        }).then(response => {
          console.log(response.data.code);
          if (response.data.code == 200) {
            wx.setStorageSync('token', response.data.token)
            console.log('sss', wx.getStorageSync('token'));
          }
        })
      } else {
        console.log('登录失败！' + res.errMsg)
      }
    }
  })
}