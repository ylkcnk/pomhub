const request = require('../utils/request'); //引入刚刚编写的request 文件

const sportApi = {
  uploadRunData(data) {
    return request({
      url: `/pgh/sport/uploadRunData`,
      method: 'POST',
      data: data
    });
  },
}


module.exports = sportApi