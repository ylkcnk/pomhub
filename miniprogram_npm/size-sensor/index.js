module.exports = (function() {
var __MODS__ = {};
var __DEFINE__ = function(modId, func, req) { var m = { exports: {}, _tempexports: {} }; __MODS__[modId] = { status: 0, func: func, req: req, m: m }; };
var __REQUIRE__ = function(modId, source) { if(!__MODS__[modId]) return require(source); if(!__MODS__[modId].status) { var m = __MODS__[modId].m; m._exports = m._tempexports; var desp = Object.getOwnPropertyDescriptor(m, "exports"); if (desp && desp.configurable) Object.defineProperty(m, "exports", { set: function (val) { if(typeof val === "object" && val !== m._exports) { m._exports.__proto__ = val.__proto__; Object.keys(val).forEach(function (k) { m._exports[k] = val[k]; }); } m._tempexports = val }, get: function () { return m._tempexports; } }); __MODS__[modId].status = 1; __MODS__[modId].func(__MODS__[modId].req, m, m.exports); } return __MODS__[modId].m.exports; };
var __REQUIRE_WILDCARD__ = function(obj) { if(obj && obj.__esModule) { return obj; } else { var newObj = {}; if(obj != null) { for(var k in obj) { if (Object.prototype.hasOwnProperty.call(obj, k)) newObj[k] = obj[k]; } } newObj.default = obj; return newObj; } };
var __REQUIRE_DEFAULT__ = function(obj) { return obj && obj.__esModule ? obj.default : obj; };
__DEFINE__(1710471431343, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ver = exports.clear = exports.bind = void 0;

var _sensorPool = require("./sensorPool");

/**
 * Created by hustcc on 18/6/9.[高考时间]
 * Contract: i@hust.cc
 */

/**
 * bind an element with resize callback function
 * @param {*} element
 * @param {*} cb
 */
var bind = function bind(element, cb) {
  var sensor = (0, _sensorPool.getSensor)(element); // listen with callback

  sensor.bind(cb); // return unbind function

  return function () {
    sensor.unbind(cb);
  };
};
/**
 * clear all the listener and sensor of an element
 * @param element
 */


exports.bind = bind;

var clear = function clear(element) {
  var sensor = (0, _sensorPool.getSensor)(element);
  (0, _sensorPool.removeSensor)(sensor);
};

exports.clear = clear;
var ver = "0.2.7";
exports.ver = ver;
}, function(modId) {var map = {"./sensorPool":1710471431344}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431344, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeSensor = exports.getSensor = void 0;

var _id = _interopRequireDefault(require("./id"));

var _sensors = require("./sensors");

var _constant = require("./constant");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Created by hustcc on 18/6/9.
 * Contract: i@hust.cc
 */

/**
 * all the sensor objects.
 * sensor pool
 */
var Sensors = {};
/**
 * get one sensor
 * @param element
 * @returns {*}
 */

var getSensor = function getSensor(element) {
  var sensorId = element.getAttribute(_constant.SizeSensorId); // 1. if the sensor exists, then use it

  if (sensorId && Sensors[sensorId]) {
    return Sensors[sensorId];
  } // 2. not exist, then create one


  var newId = (0, _id["default"])();
  element.setAttribute(_constant.SizeSensorId, newId);
  var sensor = (0, _sensors.createSensor)(element); // add sensor into pool

  Sensors[newId] = sensor;
  return sensor;
};
/**
 * 移除 sensor
 * @param sensor
 */


exports.getSensor = getSensor;

var removeSensor = function removeSensor(sensor) {
  var sensorId = sensor.element.getAttribute(_constant.SizeSensorId); // remove attribute

  sensor.element.removeAttribute(_constant.SizeSensorId); // remove event, dom of the sensor used

  sensor.destroy(); // exist, then remove from pool

  if (sensorId && Sensors[sensorId]) {
    delete Sensors[sensorId];
  }
};

exports.removeSensor = removeSensor;
}, function(modId) { var map = {"./id":1710471431345,"./sensors":1710471431346,"./constant":1710471431349}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431345, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Created by hustcc on 18/6/9.
 * Contract: i@hust.cc
 */
var id = 1;
/**
 * generate unique id in application
 * @return {string}
 */

var _default = function _default() {
  return "".concat(id++);
};

exports["default"] = _default;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431346, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createSensor = void 0;

var _object = require("./object");

var _resizeObserver = require("./resizeObserver");

/**
 * Created by hustcc on 18/7/5.
 * Contract: i@hust.cc
 */

/**
 * sensor strategies
 */
// export const createSensor = createObjectSensor;
var createSensor = typeof ResizeObserver !== 'undefined' ? _resizeObserver.createSensor : _object.createSensor;
exports.createSensor = createSensor;
}, function(modId) { var map = {"./object":1710471431347,"./resizeObserver":1710471431350}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431347, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createSensor = void 0;

var _debounce = _interopRequireDefault(require("../debounce"));

var _constant = require("../constant");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Created by hustcc on 18/6/9.
 * Contract: i@hust.cc
 */
var createSensor = function createSensor(element) {
  var sensor = undefined; // callback

  var listeners = [];
  /**
   * create object DOM of sensor
   * @returns {HTMLObjectElement}
   */

  var newSensor = function newSensor() {
    // adjust style
    if (getComputedStyle(element).position === 'static') {
      element.style.position = 'relative';
    }

    var obj = document.createElement('object');

    obj.onload = function () {
      obj.contentDocument.defaultView.addEventListener('resize', resizeListener); // 直接触发一次 resize

      resizeListener();
    };

    obj.setAttribute('style', _constant.SensorStyle);
    obj.setAttribute('class', _constant.SensorClassName);
    obj.setAttribute('tabindex', _constant.SensorTabIndex);
    obj.type = 'text/html'; // append into dom

    element.appendChild(obj); // for ie, should set data attribute delay, or will be white screen

    obj.data = 'about:blank';
    return obj;
  };
  /**
   * trigger listeners
   */


  var resizeListener = (0, _debounce["default"])(function () {
    // trigger all listener
    listeners.forEach(function (listener) {
      listener(element);
    });
  });
  /**
   * listen with one callback function
   * @param cb
   */

  var bind = function bind(cb) {
    // if not exist sensor, then create one
    if (!sensor) {
      sensor = newSensor();
    }

    if (listeners.indexOf(cb) === -1) {
      listeners.push(cb);
    }
  };
  /**
   * destroy all
   */


  var destroy = function destroy() {
    if (sensor && sensor.parentNode) {
      if (sensor.contentDocument) {
        // remote event
        sensor.contentDocument.defaultView.removeEventListener('resize', resizeListener);
      } // remove dom


      sensor.parentNode.removeChild(sensor); // initial variable

      sensor = undefined;
      listeners = [];
    }
  };
  /**
   * cancel listener bind
   * @param cb
   */


  var unbind = function unbind(cb) {
    var idx = listeners.indexOf(cb);

    if (idx !== -1) {
      listeners.splice(idx, 1);
    } // no listener, and sensor is exist
    // then destroy the sensor


    if (listeners.length === 0 && sensor) {
      destroy();
    }
  };

  return {
    element: element,
    bind: bind,
    destroy: destroy,
    unbind: unbind
  };
};

exports.createSensor = createSensor;
}, function(modId) { var map = {"../debounce":1710471431348,"../constant":1710471431349}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431348, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

/**
 * Created by hustcc on 18/6/9.
 * Contract: i@hust.cc
 */
var _default = function _default(fn) {
  var delay = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 60;
  var timer = null;
  return function () {
    var _this = this;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(_this, args);
    }, delay);
  };
};

exports["default"] = _default;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431349, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SensorTabIndex = exports.SensorClassName = exports.SensorStyle = exports.SizeSensorId = void 0;

/**
 * Created by hustcc on 18/6/9.
 * Contract: i@hust.cc
 */
var SizeSensorId = 'size-sensor-id';
exports.SizeSensorId = SizeSensorId;
var SensorStyle = 'display:block;position:absolute;top:0;left:0;height:100%;width:auto;overflow:hidden;pointer-events:none;z-index:-1;opacity:0';
exports.SensorStyle = SensorStyle;
var SensorClassName = 'size-sensor-object';
exports.SensorClassName = SensorClassName;
var SensorTabIndex = '-1';
exports.SensorTabIndex = SensorTabIndex;
}, function(modId) { var map = {}; return __REQUIRE__(map[modId], modId); })
__DEFINE__(1710471431350, function(require, module, exports) {


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createSensor = void 0;

var _debounce = _interopRequireDefault(require("../debounce"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * Created by hustcc on 18/7/5.
 * Contract: i@hust.cc
 */
var createSensor = function createSensor(element) {
  var sensor = undefined; // callback

  var listeners = [];
  /**
   * trigger listeners
   */

  var resizeListener = (0, _debounce["default"])(function () {
    // trigger all
    listeners.forEach(function (listener) {
      listener(element);
    });
  });
  /**
   * create ResizeObserver sensor
   * @returns
   */

  var newSensor = function newSensor() {
    var s = new ResizeObserver(resizeListener); // listen element

    s.observe(element); // trigger once

    resizeListener();
    return s;
  };
  /**
   * listen with callback
   * @param cb
   */


  var bind = function bind(cb) {
    if (!sensor) {
      sensor = newSensor();
    }

    if (listeners.indexOf(cb) === -1) {
      listeners.push(cb);
    }
  };
  /**
   * destroy
   */


  var destroy = function destroy() {
    sensor.disconnect();
    listeners = [];
    sensor = undefined;
  };
  /**
   * cancel bind
   * @param cb
   */


  var unbind = function unbind(cb) {
    var idx = listeners.indexOf(cb);

    if (idx !== -1) {
      listeners.splice(idx, 1);
    } // no listener, and sensor is exist
    // then destroy the sensor


    if (listeners.length === 0 && sensor) {
      destroy();
    }
  };

  return {
    element: element,
    bind: bind,
    destroy: destroy,
    unbind: unbind
  };
};

exports.createSensor = createSensor;
}, function(modId) { var map = {"../debounce":1710471431348}; return __REQUIRE__(map[modId], modId); })
return __REQUIRE__(1710471431343);
})()
//miniprogram-npm-outsideDeps=[]
//# sourceMappingURL=index.js.map