// components/stickyNotes/stickyNotes.js

import todoApi from "../../api/todo.js";
const app = getApp()
var util = require('../../utils/util')

var binds = require('../../utils/binds')

const mins = []
for (let i = 5; i <= 180; i = i + 5) {
  mins.push(i)
}
Component(Object.assign({
  /**
   * 组件的属性列表
   */

  properties: {

  },
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的初始数据
   */
  data: {

    //时间
    duration: 0,

    //查询集合
    todoList: [],
    //todoform
    todoform: {
      createTime: null,
      duration: null,
      isdone: 0,
      name: null,
      parentId: null,
      remainingDuration: null,
      remark: null,
      todoTimes: null,
      updateTime: null,
      userId: null,
    },

    todoId: '',

    todoName: "",
    listName: "",
    title: [0],
    activeNames: ['1'],

    colorList: app.globalData.ColorList.slice(0, 8),

    plans: [],
    loading: true,
    modalName: false,
    mins,
    min: -1,
    value: [9999, 1, 1],
    inputValue: "",
    listname: "",
    isPlans: true,
    isTodoList: true,
    plansId: 0,
    colors: [
      "red", "orange", "olive", "green",
      "cyan", "mauve", "purple"
    ],

  },


  lifetimes: {
    attached: function () {
      //获取主题色
      this.setData({
        isTodoList: true,
        themeColor: wx.getStorageSync('themeColor'),
        // todoList: wx.getStorageSync('todoList')
      })
      this.onLoad()
    },

  },


  /**
   * 组件的方法列表
   */
  methods: {

    /**
     * 生命周期函数--监听页面加载
     */
    handleButtonClick(e) {
      e.stopPropagation(); // 阻止事件冒泡
      // 这里执行您想要的点击按钮时的操作
      this.test();
    },
    test() {
      console.log("asdajks");
    },
    onChange(event) {
      this.setData({
        activeNames: event.detail,
      });
    },

    handleDital(id) {
      todoApi.getList(id).then(res => {

        this.setData({
          todoform: res.data.data

        })
      })
    },

    selectTodoList() {
      todoApi.todolist().then(res => {
        if (res.data.code === 401) {
          console.log("401");
        }
        if (res.data.rows.length == 0) {
          this.setData({
            isTodoList: false
          })
        } else {
          this.setData({
            isTodoList: true,
            todoList: res.data.rows
          })
        }

      })
    },
    selectTodoListChild() {
      todoApi.todolistChild().then(res => {

        if (res.data.rows.length == 0) {
          this.setData({
            isTodoList: false
          })
        } else {
          var p = res.data.rows
          wx.setStorageSync('plans', p)

        }
      })
    },


    onLoad() {
      this.selectTodoList()
      this.selectTodoListChild()
    },
    /**
     * 获取当前时间
     */
    getDate: function () {
      return util.formatDate(new Date())
    },

    /**
     * 添加待办集(本地储存)
     */
    // addList: function () {
    //   this.setData({
    //     isPlansList: true,
    //     showPlan: false
    //   })

    //   var p = wx.getStorageSync('todoList') || []
    //   this.setData({
    //     todoList: p
    //   })
    //   var todoL = {
    //     id: new Date().getTime(),
    //     event: this.data.listname,
    //     time: this.getDate()
    //   }
    //   p.push(todoL)
    //   wx.setStorageSync('todoList', p)

    //   this.setData({
    //     modalName: null
    //   })
    //   this.onLoad();
    // },
    /**
     * 添加待办集
     */
    // 简易双向数据绑定
    handInputChange(e) {
      this.setData({
        [`todoform.${e.target.id}`]: e.detail.value
      })
    },

    addList: function () {
      this.setData({
        isPlansList: true,
        showPlan: false,

      })
      var todoL = {
        name: this.data.listname,
        userId: 1,
      }
      if (this.data.listname == '') {
        wx.showToast({
          title: '请输入待办集名称',
          icon: 'error',
          duration: 1000
        })
        return
      }
      todoApi.addList(todoL).then(res => {
        if (res.data.code == 200) {
          this.selectTodoList()
          wx.showToast({
            title: '添加成功',
            icon: 'success',
            duration: 1000
          })
        } else {
          wx.showToast({
            title: '操作失败',
            icon: 'error',
            duration: 1000
          })
        }

      })
      this.setData({
        modalName: null
      })

    },

    /**
     * 添加计划
     */
    addPlan: function () {
      this.setData({
        isPlans: true,
        showPlan: false
      })

      // var p = wx.getStorageSync('plans') || []
      // this.setData({
      //   plans: p
      // })
      if (this.data.min == -1) {
        this.setData({
          min: 35
        })
      }
      var plan = {
        duration: (this.data.min + 1) * 5,
        remainingDuration: (this.data.min + 1) * 5,
        name: this.data.inputValue,
        userId: 1,
        parentId: this.data.todoId
      }
      if (this.data.inputValue == '') {
        wx.showToast({
          title: '请输入名称',
          icon: 'error',
          duration: 1000
        })
        return
      }
      todoApi.addList(plan).then(res => {
        if (res.data.code == 200) {
          this.selectTodoList()
          wx.showToast({
            title: '添加成功',
            icon: 'success',
            duration: 1000
          })
        } else {
          wx.showToast({
            title: '操作失败',
            icon: 'error',
            duration: 1000
          })
        }
      })
      // console.log("asd", plan);
      // p.push(plan)
      // wx.setStorageSync('plans', p)
      this.setData({
        modalName: null
      })

    },


    /**
     * 编辑代办
     */
    editListName: function () {

      if (this.data.todoform.name == '') {
        wx.showToast({
          title: '请输入名称',
          icon: 'error',
          duration: 1000
        })
        return
      }
      todoApi.updateList(this.data.todoform).then(res => {
        if (res.data.code == 200) {
          this.selectTodoList()
          wx.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 1000
          })
          this.onLoad();
        } else {
          wx.showToast({
            title: '操作失败',
            icon: 'error',
            duration: 1000
          })
        }
      })
      this.setData({
        modalName: null
      })
    },
    // 编辑todo
    editTodo: function () {
      if (this.data.todoform.name == '') {
        wx.showToast({
          title: '请输入名称',
          icon: 'error',
          duration: 1000
        })
        return
      }
      if (this.data.todoform.duration == '') {
        wx.showToast({
          title: '请输入时长',
          icon: 'error',
          duration: 1000
        })
        return
      }
      //如果duration是字符串
      if (typeof this.data.todoform.duration == 'string') {
        wx.showToast({
          title: '请输入数字',
          icon: 'error',
          duration: 1000
        })
        return
      }

      if (this.data.todoform.duration) {
        wx.showToast({
          title: '请输入时长',
          icon: 'error',
          duration: 1000
        })
        return
      }

      todoApi.updateList(this.data.todoform).then(res => {
        if (res.data.code == 200) {
          this.selectTodoList()
          wx.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 1000
          })
          this.onLoad();
        } else {
          wx.showToast({
            title: '操作失败',
            icon: 'error',
            duration: 1000
          })
        }
      })
      this.setData({
        modalName: null
      })
    },





    /**
     * 删除计划
     */
    deletePlan: function () {
      todoApi.delList(this.data.todoId).then(res => {
        if (res.data.code == 200) {
          this.selectTodoList()
          wx.showToast({
            title: '删除成功',
            icon: 'success',
            duration: 1000
          })
        } else {
          wx.showToast({
            title: '操作失败',
            icon: 'error',
            duration: 1000
          })
        }
      })
      this.setData({
        modalName: null
      })
      // wx.setStorageSync('plans', tPlans)

      // //首先从存储中获取所有计划
      // var tPlans = wx.getStorageSync('plans') || []
      // //根据当前选中的计划ID找到对应的计划
      // var tPlan = this.data.plans[this.data.plansId]

      // let len = tPlans.length
      // for (var i = 0; i < len; i++) {
      //   //如果计划的日期与当前日期不匹配，则从列表中删除该计划
      //   if (tPlans[i].time != this.getDate()) {
      //     tPlans.splice(i, 1)
      //     len = len - 1
      //     i = i - 1
      //     continue
      //   } else {
      //     //唯一id匹配，则同样删除该计划
      //     if (tPlans[i].id == tPlan.id) {
      //       tPlans.splice(i, 1)
      //       len = len - 1
      //       i = i - 1
      //       continue
      //     }

      //     // if (tPlans[i].event == tPlan.event && tPlans[i].duration == tPlan.duration) {
      //     //   tPlans.splice(i, 1)
      //     //   len = len - 1
      //     //   i = i - 1
      //     //   continue
      //     // }
      //   }
      // }


    },

    /**
     * 删除待办集合
     */
    deletePlanList: function () {
      //首先从存储中获取所有计划
      var todoList = this.data.todoList

      console.log(todoList)
      //根据当前选中的计划ID找到对应的计划
      var todoL = this.data.todoList[this.data.plansId]
      console.log(todoL);
      let len = todoList.length
      var index = this.data.plansId
      todoList.splice(index, 1)
      this.setData({
        modalName: null
      })
      wx.setStorageSync('todoList', todoList)
      this.selectTodoList()
      // for (var i = 0; i < len; i++) {
      //   //如果计划的日期与当前日期不匹配，则从列表中删除该计划
      //   if (todoList[i].time != this.getDate()) {
      //     todoList.splice(i, 1)
      //     len = len - 1
      //     i = i - 1
      //     continue
      //   } else {
      //     //唯一id匹配，则同样删除该计划
      //     if (todoList[i].id == todoL.id) {
      //       todoList.splice(i, 1)
      //       len = len - 1
      //       i = i - 1
      //       continue
      //     }
      //   }
      // }
      // this.setData({
      //   modalName: null
      // })
      // wx.setStorageSync('todoList', todoList)
      // this.onLoad();
    },

    /**
     * 模态框选择
     * @param {*} e 
     */

    showModal(e) {
      var id = e.currentTarget.dataset.id
      var target = e.currentTarget.dataset.target
      if (target === "editlist" || target === "editTodo") {
        this.handleDital(id)


      }
      // if (target === "editlist") {
      //   this.setData({
      //     listName: this.data.todoList[index].event,
      //   })
      // }
      // if (target === "editTodo") {
      //   this.setData({
      //     todoName: this.data.plans[index].event,
      //     value: this.data.plans[index].duration / 5 - 1
      //   })
      //   console.log(this.data.todoName);
      //   console.log(this.data.value);
      // }
      this.setData({
        modalName: e.currentTarget.dataset.target,
        todoId: e.currentTarget.dataset.id
      })

    },

    /**
     * 关闭模态框
     * @param {*} e 
     */
    hideModal(e) {
      this.setData({
        modalName: null
      })
    },

    /**
     * 选择条picker-view的min数据
     * @param {*} e 
     */
    bindChange(e) {
      const val = e.detail.value
      this.setData({
        min: val[0]
      })
    },

    /**
     * 输入框数据
     * @param {*} e 
     */
    bindKeyInput: function (e) {
      this.setData({
        inputValue: e.detail.value
      })
    },
    bindListname: function (e) {
      this.setData({
        listname: e.detail.value
      })
    },
  }
}, binds))