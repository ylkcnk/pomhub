// components/login/login.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {
      avatarUrl: ''
    },

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      themeColor: wx.getStorageSync('themeColor'),
      userInfo: wx.getStorageSync('userInfo'),
      nickName: wx.getStorageSync('nickName'),
    })
    console.log("userInfo", this.data.userInfo);
    console.log("avatarUrl", this.data.userInfo.avatarUrl);
    console.log("nickName", this.data.userInfo.nickName);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {


  },


  onChooseAvatar: function (res) {

    const {
      userInfo
    } = res.detail.avatarUrl
    this.setData({
      userInfo: res.detail
    })
    app.userInfo.avatarUrl = res.detail.avatarUrl
    wx.setStorageSync('userInfo', res.detail)
    console.log("缓存头像临时Url:", wx.getStorageSync('userInfo'))
    var filePath = getApp().userInfo.avatarUrl
    console.log('头像路径filePath:', filePath)
    var fileName = Math.random().toString(36).substr(2);
    console.log('头像图片文件名fileName:', fileName)
  },
  bindKeyInput: function (res) {
    console.log("输入框的值:", res.detail.value);
  },

  fromsubmit(e) {
    const {
      nickName
    } = e.detail.value;
    this.setData({
      nickName: e.detail.value,
    })
    console.log("nickName:", nickName)
    wx.setStorageSync('nickName', e.detail.value)
    console.log("名字有了放缓存:", wx.getStorageSync('nickName'))
  },

  setBtnTap() {
    const {
      userInfo
    } = this.data
    if ([userInfo.avatarUrl] == '/image/avatarUrl.png') {
      wx.showToast({
        title: '头像不能为空',
        icon: 'none'
      })
      return
    }
    const {
      nickName
    } = this.data
    if (!nickName) {
      wx.showToast({
        title: '昵称不能为空',
        icon: 'none'
      })
      return
    }
    wx.navigateTo({
      url: '/pages/index/index',
    })

    // wx.navigateBack({
    //   url: 'B?id=1'
    // })
  },




  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})