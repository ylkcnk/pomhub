// components/me/history/history.js
const app = getApp()
const util = require("../../../utils/util")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logs: [],
    showLogs: false,
    formatLogs: [],
    showPlan: false,
    colors: [
      "red", "orange", "olive", "green",
      "cyan", "mauve", "purple"
    ],
    isLogs: true,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ showLogs: true })
    // let l = wx.getStorageSync('logs') || []
    let l = wx.getStorageSync('awardList')

    this.setData({
      themeColor: wx.getStorageSync('themeColor')
    })
    if (l.length == 0) {
      this.setData({ isLogs: false })
    }
    this.setData({ logs: l })
    // let logs = this.data.test.map(item=>{
    //   return item+1;
    // })
    // console.log(logs)
    let fLogs = this.data.logs.map(item => {
      let startTimeTemp = new Date(item.date);
      let a = startTimeTemp.getHours() + ""; let b = startTimeTemp.getMinutes() + ""; let c = startTimeTemp.getSeconds() + "";
      if (a.length == 1) {
        a = "0" + a;
      }
      if (b.length == 1) {
        b = "0" + b;
      }
      if (c.length == 1) {
        c = "0" + c;
      }

      let startTimePart = a + " : " + b + " : " + c;
      let bigRange = startTimeTemp.getFullYear() + "/" + (startTimeTemp.getMonth() + 1) + "/" + startTimeTemp.getDate()
      let color = "gradual-green"
      let yu = item.name

      return {
        big: bigRange,
        startTime: startTimePart,
        cardColor: color,
        sign: yu,
      };
    });
    this.setData({ formatLogs: fLogs })

  },
  /**
   * 获取当前时间
   */
  getDate: function () {
    return util.formatDate(new Date())
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})