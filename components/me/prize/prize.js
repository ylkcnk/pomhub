const util = require("../../../utils/util");
import Dialog from '@vant/weapp/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    prizeTimes: 0,
    selector: '',
    Popshow: false,
    inputValue: "",
    show: false,
    //弹框名
    modalName: '',
    index: -1,
    picker: ['喵喵喵', '汪汪汪', '哼唧哼唧'],

    //获奖记录
    awardList: [{
      id: '',
      type: '',
      name: '',
      date: ''
    }, ],
    //奖品
    prize_id: "",
    prize_edit: "",
    prize_title: '',
    name_list: [],
    prize_de: [{
        id: 1,
        type: 'diy',
        name: '奖品1',
        title: '奖品1',
        isSelected: false
      },
      {
        id: 2,
        type: 'diy',
        name: '奖品2',
        title: '奖品2',
        isSelected: false
      },
      {
        id: 3,
        type: 'diy',
        name: '奖品3',
        title: '奖品3',
        isSelected: false
      },
      {
        id: 4,
        type: 'diy',
        name: '奖品8',
        title: '奖品8',
        isSelected: false
      },
      {
        id: 5,
        type: 'btn',
        name: '开抽',
        title: '按钮',
        isSelected: false
      },
      {
        id: 6,
        type: 'prize',
        name: '背景',
        title: '奖品4',
        isSelected: false
      },
      {
        id: 7,
        type: 'prize',
        name: '头像框',
        title: '奖品7',
        isSelected: false
      },
      {
        id: 8,
        type: 'diy',
        name: '奖品6',
        title: '奖品6',
        isSelected: false
      },
      {
        id: 9,
        type: 'diy',
        name: '奖品5',
        title: '奖品5',
        isSelected: false
      },
    ],
    prize_arr: [],
    mid: -1,
    /*
     * 数组的长度就是最多所转的圈数，最后一圈会转到中奖后的位置
     * 数组里面的数字表示从一个奖品跳到另一个奖品所需要的时间
     * 数字越小速度越快
     * 想要修改圈数和速度的，更改数组个数和大小即可
     */
    num_interval_arr: [90, 80, 70, 60, 50, 50, 50, 100, 150, 250],
    // num_interval_arr: [90, 80],
    /*
     * 页面中奖项的实际数组下标
     * 0  1  2
     * 3     5
     * 6  7  8
     * 所以得出转圈的执行顺序数组为 ↓
     */
    order_arr: [0, 1, 2, 5, 8, 7, 6, 3],
    // 抽奖状态，是否转完了
    isTurnOver: true,
    // 是否需要复原，把没转完的圈转完
    isRecover: false,
    // 记录上一次抽奖后的奖品id
    prize_id_last: ''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var prize_arr = wx.getStorageSync('prize_arr') || []
    if (!wx.getStorageSync("prizeTimes")) {
      wx.setStorageSync('prizeTimes', 10)
      this.setData({
        prizeTimes: 10
      })
    } else {
      this.setData({
        prizeTimes: wx.getStorageSync('prizeTimes')
      })
    }
    this.setData({
      themeColor: wx.getStorageSync('themeColor'),
      prizeTimes: wx.getStorageSync('prizeTimes')
    })
    // wx.setStorageSync('prize_arr', this.data.prize_de)
    if (prize_arr.length > 0) {
      this.setData({
        prize_arr: prize_arr
      })
    } else {
      this.setData({
        prize_arr: this.data.prize_de
      })

    }
  },

  showMag(e) {
    this.setData({
      modalName: e.currentTarget.dataset.target,
      show: true,
    })
  },

  showModal(e) {
    var index = e.currentTarget.dataset.id - 1;
    var prize_edit = this.data.prize_arr[index].name;
    this.setData({
      modalName: e.currentTarget.dataset.target,
      show: true,
      prize_id: index,
      prize_edit: prize_edit,
      prize_title: this.data.prize_arr[index].title
    })
  },


  //编辑
  change() {
    const prize_arr = this.data.prize_arr || []
    const index = this.data.prize_id
    prize_arr[index].name = this.data.prize_edit

    this.setData({
      prize_arr: prize_arr
    })
    wx.setStorageSync('prize_arr', prize_arr)
    this.hideModal()
  },


  /**
   * 关闭模态框
   * @param {*} e 
   */
  hideModal(e) {
    this.setData({
      modalName: null
    })
  },

  bindPrize: function (e) {
    this.setData({
      prize_edit: e.detail.value
    })

  },

  showMag() {
    this.setData({
      Popshow: true
    })
  },
  onClose() {
    this.setData({
      Popshow: false
    })
  },

  bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value,
      mid: e.detail.value,

    })
  },



  // 点击抽奖
  clickPrize() {
    if (this.data.prizeTimes <= 0) {
      wx.showToast({
        title: '抽奖次数不足，请自律',
        icon: 'none'
      })
      return
    }

    // 如果不在抽奖状态中，则执行抽奖旋转动画
    if (this.data.isTurnOver) {
      // 把抽奖状态改为未完成
      this.setData({
        isTurnOver: false,
        prizeTimes: this.data.prizeTimes - 1
      })
      // 这里开始假设已经调取后端接口拿到抽奖后返回的ID
      // let prize_id = 8;

      // 随机奖品效果
      let numbers = this.data.order_arr
      let length = numbers.length;
      // 生成一个随机索引
      let randomIndex = Math.floor(Math.random() * length);
      // 使用随机索引从数组中选取一个元素
      let randomNumber = numbers[randomIndex];
      // const rand = (m, n) => {
      //   return Math.ceil(Math.random() * (n - m + 1) + m - 1)
      // }
      let prize_id = randomNumber
      console.log(prize_id);
      if (this.data.mid != -1) {
        prize_id = Number(this.data.mid) + 1

      }
      // this.setData({
      //   isTurnOver: true
      // })
      // 调用抽奖方法
      this.lottery(prize_id);
    } else {
      wx.showToast({
        title: '请勿重复点击',
        icon: 'none'
      })
    }
  },

  // 抽奖旋转动画方法
  async lottery(prize_id) {
    console.log('中奖ID：' + prize_id)
    // 如果不是第一次抽奖，需要等待上一圈没跑完的次数跑完再执行
    this.recover().then(() => {
      let num_interval_arr = this.data.num_interval_arr;
      let order_arr = this.data.order_arr;
      // 旋转的总次数
      let sum_rotate = num_interval_arr.length;
      // 每一圈所需要的时间
      let interval = 0;
      num_interval_arr.forEach((delay, index) => {
        setTimeout(() => {
          this.rotateCircle(delay, index + 1, sum_rotate, prize_id, order_arr);
        }, interval)
        //因为每一圈转完所用的时间是不一样的，所以要做一个叠加操作
        interval += delay * 8;
      })
    })
  },

  /*
   * 封装旋转一圈的动画函数，最后一圈可能不满一圈
   * delay:表示一个奖品跳到另一个奖品所需要的时间
   * index:表示执行到第几圈
   * sum_rotate：表示旋转的总圈数
   * prize_id：中奖后的id号
   * order_arr_pre：表示旋转这一圈的执行顺序
   */
  rotateCircle(delay, index, sum_rotate, prize_id, order_arr_pre) {
    // console.log(index)
    let _this = this;
    // 页面奖品总数组
    let prize_arr = this.data.prize_arr || [];
    // 执行顺序数组
    let order_arr = []
    // 如果转到最后以前，把数组截取到奖品项的位置
    if (index == sum_rotate) {
      order_arr = order_arr_pre.slice(0, prize_id)
    } else {
      order_arr = order_arr_pre;
    }
    for (let i = 0; i < order_arr.length; i++) {
      setTimeout(() => {
        // 清理掉选中的转态
        prize_arr.forEach(e => {
          e.isSelected = false
        })
        // 执行到第几个就改变它的选中状态
        prize_arr[order_arr[i]].isSelected = true;
        // 更新状态
        _this.setData({
          prize_arr: prize_arr
        })
        // 如果转到最后一圈且转完了，并且是非重置圈，把抽奖状态改为已经转完了
        if (index === sum_rotate && i === order_arr.length - 1 && !this.data.isRecover) {
          _this.setData({
            isTurnOver: true,
            isRecover: true,
            prize_id_last: prize_id,
            mid: -1,
            value: -1
          })
          _this.handlePrize()
        }
      }, delay * i)
    }
  },

  // 复原，把上一次抽奖没跑完的次数跑完
  async recover() {
    if (this.data.isRecover) { // 判断是否需要重置操作
      let delay = this.data.num_interval_arr[0]; // 为了衔接流程，使用第一圈的转速
      // console.log(delay)
      let order_arr = this.data.order_arr;
      // console.log(order_arr)
      let prize_id_last = this.data.prize_id_last; // 上一次抽奖的id
      // console.log(prize_id_last)
      order_arr = order_arr.slice(prize_id_last); // 截取未跑完的格子数组
      // console.log(order_arr)
      return await new Promise(resolve => { // 确保跑完后才去执行新的抽奖
        this.rotateCircle(delay, 1, 1, 8, order_arr); // 第一圈的速度，最多只有一圈，旋转一圈，跑到最后一个奖品为止，未跑完的数组
        setTimeout(() => { // 确保跑完后才告诉程序不用重置复原了
          this.setData({
            isRecover: false,
          })
          resolve() // 告诉程序Promise执行完了
        }, delay * order_arr.length)
      })
    }
  },
  getDate: function () {
    return util.format(new Date())
  },


  // 处理中奖
  handlePrize() {
    var pid = this.data.prize_id_last - 1
    var Rid = this.data.order_arr[pid]
    console.log('pid', pid);
    console.log("Rid", Rid);

    var awardL = wx.getStorageSync('awardList') || []
    var award = {
      id: new Date().getTime(),
      prizeId: pid,
      type: this.data.prize_arr[Rid].type,
      name: this.data.prize_arr[Rid].name,
      date: this.getDate()
    }
    awardL.push(award)
    wx.setStorageSync('awardList', awardL)
    Dialog.alert({
      title: '恭喜',
      message: '您为您自己赢得了' + award.name,

    }).then(() => {
      // on close
    });
  }

})