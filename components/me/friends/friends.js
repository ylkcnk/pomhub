// components/me/friends/friends.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

    hidden: true,
    listCurID: '',
    list: [],
    listCur: '',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //获取主题色
    this.setData({
      themeColor: wx.getStorageSync('themeColor')
    })
    console.log(this.data.themeColor);
    let list = [{}];
    for (let i = 0; i < 26; i++) {
      list[i] = {};
      list[i].name = String.fromCharCode(65 + i);
    }
    this.setData({
      list: list,
      listCur: list[0]

    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // let that = this;
    //这两句大概知道什么意思，但是emmm,这个boxTop和barTop具体是什么数值，不是很理解
    // uni.createSelectorQuery().select('.indexBar-box').boundingClientRect(function (res) {
    //   that.boxTop = res.top
    // }).exec();
    // uni.createSelectorQuery().select('.indexes').boundingClientRect(function (res) {
    //   that.barTop = res.top
    // }).exec()

  },
  methods: {
    //获取文字信息
    getCur(e) {
      this.hidden = false;
      this.listCur = this.list[e.target.id].name;
    },
    setCur(e) {
      this.hidden = true;
      this.listCur = this.listCur
    },
    //滑动选择Item
    tMove(e) {
      console.log(this.boxTop)
      let y = e.touches[0].clientY,
        offsettop = this.boxTop,
        that = this;
      //判断选择区域,只有在选择区才会生效
      if (y > offsettop) {
        // 这个num计算结果怎么就是字母列表的下标呢，我没想明白，望大佬指教
        let num = parseInt((y - offsettop) / 20);
        this.listCur = that.list[num].name
      };
    },

    //触发全部开始选择
    tStart() {
      this.hidden = false
    },

    //触发结束选择
    tEnd() {
      this.hidden = true;
      this.listCurID = this.listCur
    },
    //源码中有这个函数，但是这个页面中根本没有用到，删了没影响，我也不明白作者为什么写这个
    indexSelect(e) {
      let that = this;
      let barHeight = this.barHeight;
      let list = this.list;
      let scrollY = Math.ceil(list.length * e.detail.y / barHeight);
      for (let i = 0; i < list.length; i++) {
        if (scrollY < i + 1) {
          that.listCur = list[i].name;
          that.movableY = i * 20
          return false
        }
      }
    }


  },



  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})