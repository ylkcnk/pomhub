// components/me/editFrame/editFrame.js
const app = getApp()
const util = require("../../../utils/util")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    logs: [],
    showLogs: false,
    formatLogs: [],
    showPlan: false,
    colors: [
      "red", "orange", "olive", "green",
      "cyan", "mauve", "purple"
    ],
    isLogs: true,

    avatarUrl: wx.getStorageSync('userInfo').avatarUrl, // 用户头像URL
    frameList: app.globalData.frameList,
    selectedFrameUrl: wx.getStorageSync('FrameUrl'), // 默认选中的头像框
    selectedIndex: wx.getStorageSync('selectedIndex'),


  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      showLogs: true
    })
    this.setData({
      themeColor: wx.getStorageSync('themeColor'),
      selectedFrameUrl: wx.getStorageSync('FrameUrl'),
      selectedIndex: wx.getStorageSync('selectedIndex'),
      avatarUrl: wx.getStorageSync('userInfo').avatarUrl
    })

  },
  /**
   * 获取当前时间
   */
  getDate: function () {
    return util.formatDate(new Date())
  },
  selectFrame: function (event) {
    const index = event.currentTarget.dataset.index;
    this.setData({
      selectedFrameUrl: this.data.frameList[index].imageUrl,
      selectedIndex: index,
    });

  },
  save() {
    wx.setStorageSync('FrameUrl', this.data.selectedFrameUrl)
    wx.setStorageSync('selectedIndex', this.data.selectedIndex)
    wx.showToast({
      title: '选择成功',
      icon: 'success',
      duration: 1000,
      mask: true
    })

    setTimeout(() => {
      wx.navigateTo({
        url: '/pages/index/index',
      })
    }, 1500);
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})