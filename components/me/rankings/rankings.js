// components/me/rankings/rankings.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ranklist: [{
        name: '程俊博',
        pic: 'https://cdn.airbozh.cn/blog/info.jpg',
        score: '1162'
      },
      {
        name: '房乐朋',
        pic: 'https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cHM6Ly9jLXNzbC5kdWl0YW5nLmNvbS91cGxvYWRzL2l0ZW0vMjAyMDA3LzAxLzIwMjAwNzAxMTEyNDM4X21pd2dnLnRodW1iLjEwMDBfMC5qcGc=&sign=yx:YRyJPE6qlM8Sv3VbdkeBiVhhI1g=&tv=0_0',
        score: '1087'
      },
      {
        name: '苏镜扬',
        pic: 'https://cdn.airbozh.cn/blog/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20240518153042.jpg',
        score: '1021'
      },
      {
        name: '张祖晖',
        pic: 'https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cHM6Ly9zbnMtaW1nLWh3Lnhoc2Nkbi5uZXQvMTA0MGcwMDgzMG8ydWY1Nm1pYzZnNW9uMHA0bm5xdG1pOW4yZzUzbw==&sign=yx:_RsXkfhMYZemuPMOeSX8pbX6Ljc=&tv=0_0',
        score: '920'
      },
      {
        name: 'Airbo',
        pic: ' https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cDovL2kuamYyNTguY29tL3VwLzAvMjUzLzMxMTc3MzkzMjIvOTc5Mjk2MTIzLmpwZw==&sign=yx:-59DJ20OY0KtnR5t5N7JWmBrgCw=&tv=0_0',
        score: '859'
      },
      {
        name: '北槐',
        pic: ' https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cDovL3NhZmUtaW1nLnhoc2Nkbi5jb20vYncxL2JhNmJhODI5LTM1NzAtNDMxZC1hMmRjLWNkYTZkNWYyMWVmZj9pbWFnZVZpZXcyLzIvdy8xMDgwL2Zvcm1hdC9qcGc=&sign=yx:pa4Kcm8d5s3XeVcrBkfc4DcPqpI=&tv=0_0',
        score: '821'
      },
      {
        name: 'DumB',
        pic: 'https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cHM6Ly9zbnMtaW1nLWh3Lnhoc2Nkbi5uZXQvN2RkMjhjZDQtZmE1My1kZGVkLTBlODYtNzQ4ZjZiZTE5MDVi&sign=yx:NrzU5w6IMmFo9tzDuPShz2CeWPg=&tv=0_0',
        score: '799'
      },
      {
        name: '狐瞳瞳',
        pic: 'https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cDovL2ltZy5jcmN6LmNvbS9hbGxpbWcvMjAxOTEwLzMxLzE1NzI0OTU4ODc4OTMzMzkuanBn&sign=yx:uEietQG-gm4w9gxp9NyL5BIeEds=&tv=0_0',
        score: '620'
      },
      {
        name: '软萌猫耳酱',
        pic: 'https://kkimgs.yisou.com/ims?kt=url&at=ori&key=aHR0cDovL2ltZy5haWdleGluZy5jb20vdXBsb2Fkcy84LzEyNTMvNDE2NDg5NzM3OS85Mjk2ODk2MzIzMi81Nzc4Mjc2MjUuanBn&sign=yx:MxRUViA20dxa-8NfMetRW2Jo_Mg=&tv=0_0',
        score: '365'
      }
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    //获取主题色
    this.setData({
      themeColor: wx.getStorageSync('themeColor')
    })
    console.log(this.data.themeColor);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})