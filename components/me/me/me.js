// components/me/me.js
const app = getApp()
import {
  login
} from '../../../api/user.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的初始数据
   */
  data: {
    themeModal: false,
    colorList: app.globalData.ColorList.slice(0, 8),
    hasUserInfo: false,
    canIUseGetUserProfile: false,
    userInfo: {
      avatarUrl: '',
      nickName: ''
    },
    nickName: '',
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  lifetimes: {
    attached: function () {
      if (wx.getUserProfile) {
        this.setData({
          canIUseGetUserProfile: true
        })
      }
      // 在组件实例进入页面节点树时执行
      if (wx.getStorageSync('userInfo')) {
        this.setData({
          userInfo: wx.getStorageSync('userInfo'),
          nickName: wx.getStorageSync('nickName'),
          hasUserInfo: true,
        })
      }
      //获取主题色
      this.setData({

        themeColor: wx.getStorageSync('themeColor'),
        //获取头像框
        FrameUrl: wx.getStorageSync('FrameUrl'),
        // avatarUrl: '/images/猫猫头.jpg',
        // frameImageUrl: 'https://c-ssl.dtstatic.com/uploads/item/201809/02/20180902215213_jusqy.thumb.1000_0.png'
      })


    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行

    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    sysLogin() {
      login()
    },
    getUserProfile(e) {
      app.globalData.userInfo = e.detail.userInfo
      // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
      // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
      wx.getUserProfile({
        desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
        success: (res) => {
          console.log(res.userInfo);
          this.setData({
            userInfo: res.userInfo,
            'userInfo.avatarUrl': res.userInfo.avatarUrl,
            hasUserInfo: true
          })
          wx.setStorageSync('userInfo', this.data.userInfo)
        }
      })

    },

    //获取个人信息
    getUserInfo: function (e) {
      console.log(1)
      app.globalData.userInfo = e.detail.userInfo
      this.setData({
        userInfo: e.detail.userInfo,
        hasUserInfo: true
      })
      wx.setStorageSync('userInfo', this.data.userInfo)
    },
    refresh: function () {
      // 自定义的刷新数据方法
      console.log("ahsu");
      this.loadData();
    },
    loadData: function () {
      this.setData({
        selectedFrameUrl: wx.getStorageSync('FrameUrl'),
      })
    },
    onLoad() {

      if (wx.getStorageSync('userInfo')) {
        //获取头像框
        this.setData({
          FrameUrl: wx.getStorageSync('FrameUrl'),
          userInfo: wx.getStorageSync('userInfo'),
          hasUserInfo: true,
        })

      }
    },

    //更改主题色
    hideModal(e) {
      this.setData({
        themeModal: false
      })
    },
    changeTheme() {
      this.setData({
        themeModal: true
      })

    },
    themeSelect(e) {
      let id = e.currentTarget.dataset.id
      wx.setStorageSync('themeColor', this.data.colorList[id].name)
      let flag = {
        themeColor: this.data.colorList[id].name
      }
      this.triggerEvent('themeChange', flag)
      this.setData({
        themeColor: this.data.colorList[id].name
      })
      this.hideModal()
    },
    edit() {
      //页面跳转
      wx.navigateTo({
        url: '/components/me/editFrame/editFrame'
      })
    },

    getinfo() {
      login()
      //页面跳转
      wx.navigateTo({
        url: '/components/login/login'
      })
    }








  }
})