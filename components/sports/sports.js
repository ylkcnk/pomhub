import sportApi from '../../api/sport.js'

Component({
  lifetimes: {
    attached: function () {
      wx.authorize({
        scope: 'scope.userLocation'
      })
      this.refeshLOcation(this);
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的初始数据
   */
  data: {
    location: {
      latitude: 39.13815,
      longitude: 117.15011,
    },
    polyline: [{
      points: [],
      color: '#8AC349',
      width: 10,
    }],
    marker: [],
    inter: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    refeshLOcationWrapper() {
      this.refeshLOcation(this)
    },
    run() {
      let that = this
      this.setData({
        marker: [{
          id: 0,
          width: 64,
          height: 64,
          latitude: this.data.location.latitude,
          longitude: this.data.location.longitude,
          iconPath: '/images/starpoint.png'
        }]
      })
      that.data.inter = setInterval(
        async () => {
          console.log(that.data.polyline[0].points);
          let location = await that.refeshLOcation(that)
          if (!location) return;
          let la = location.split(',')[0]
          let lo = location.split(',')[1]
          // let map = wx.createMapContext('sport-map', this)
          // console.log(map);
          if (location != "") {
            that.data.polyline[0].points.push({
              latitude: parseFloat(la),
              longitude: parseFloat(lo),
            })
            that.setData({
              polyline: that.data.polyline
            })
          }
        }, 1000);
    },
    stop() {
      clearInterval(this.data.inter)
      let markers = this.data.marker
      markers.push({
        id: 1,
        width: 64,
        height: 64,
        latitude: this.data.location.latitude,
        longitude: this.data.location.longitude,
        iconPath: '/images/endpoint.png'
      })
      this.setData({
        marker: markers
      })
      this.uploadSportData(this)
    },
    uploadSportData(that) {
      sportApi.uploadRunData(that.data.polyline[0].points)
    },
    async refeshLOcation(that) {
      let latitude = 1.0851;
      let longitude = 1.0851;

      // 定义一个返回 Promise 的函数
      function getLocation() {
        return new Promise((resolve, reject) => {
          wx.getLocation({
            type: "gcj02",
            isHighAccuracy: true,
            highAccuracyExpireTime: 60000,
            success(res) {
              console.log(res);
              latitude = res.latitude;
              longitude = res.longitude;
              resolve(); // 成功时 resolve Promise
            },
            fail(err) {
              reject(err); // 失败时 reject Promise
            }
          });
        });
      }

      try {
        await getLocation(); // 使用 await 等待异步函数完成
        // 异步函数完成后更新数据
        that.setData({
          location: {
            latitude: latitude,
            longitude: longitude,
          }
        });
        // 返回更新后的经纬度
        return latitude + ',' + longitude;
      } catch (error) {
        console.error('Failed to get location:', error);
        // 返回默认经纬度
        return latitude + ',' + longitude;
      }
    }
  }
})